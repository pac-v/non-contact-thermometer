// v2 - PAC-V Non-Contact Thermometer
// ZAID PIRWANI, EJAAD TECH, +92 322 233 9026, zaidpirwani@ejaadtech.com
// as part of COVID-19 Emergency Response
// with help from many people, Dr. Bilal, Umair, Jamal Nasim and many others
// from ORIGINAL CODE FROM: https://www.instructables.com/id/Arduino-Laser-Infrared-Thermometer/

#include "LowPower.h"

#include <Wire.h>
#include <Adafruit_MLX90614.h>
#include <Adafruit_SSD1306.h>
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
const int buttonPin = 2;     // the number of the pushbutton pin
int buttonState = 0;
// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);
Adafruit_MLX90614 mlx = Adafruit_MLX90614();

#define DEBUG 0


void wakeUp()
{
    // EMPTY HANDLER for WAKEUP INTERRUPT
}

void setup() {
  if(DEBUG){
    Serial.begin(9600);
    Serial.println("Non Contact Thermometer MLX90614 test"); 
  }
  pinMode(buttonPin, INPUT_PULLUP);
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
    if(DEBUG)Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }
  
  mlx.begin();  
  display.clearDisplay();
  display.setRotation(2);
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0, 35);
  display.println("Starting...");
  display.display();
  delay(500);
  display.clearDisplay();
  display.display();

  attachInterrupt(digitalPinToInterrupt(buttonPin), wakeUp, LOW);
  LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF); 

}

float tempC=0, tempF=0;
void loop() {
  attachInterrupt(digitalPinToInterrupt(buttonPin), wakeUp, LOW);
  LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF); 
  detachInterrupt(digitalPinToInterrupt(buttonPin));
  buttonState = digitalRead(buttonPin);
  if(DEBUG){
    Serial.println(buttonState);
    Serial.print("Ambient = "); Serial.print(mlx.readAmbientTempC()); 
    Serial.print("*C\tObject = "); Serial.print(mlx.readObjectTempC()); Serial.println("*C");
    Serial.print("Ambient = "); Serial.print(mlx.readAmbientTempF()); 
    Serial.print("*F\tObject = "); Serial.print(mlx.readObjectTempF()); Serial.println("*F");
    Serial.println();
  }
  if (buttonState == LOW) {
    tempC = mlx.readObjectTempC();
    tempF = mlx.readObjectTempF();
    display.clearDisplay();
    display.setTextSize(2);  //Size 2 means each pixel is 12 width and 16 high
    display.setCursor(25, 10);
    display.print(tempC);
    display.setCursor(95, 10);
    display.print("C");
    display.setTextSize(2);
    display.setCursor(25, 36);
    display.print(tempF);
    display.setCursor(95, 36);
    display.print("F");
    display.display();
  } else {

  }
  delay(500);

}
