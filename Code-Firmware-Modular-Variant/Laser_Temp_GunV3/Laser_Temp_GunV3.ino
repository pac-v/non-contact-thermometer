// set to 1 for 0.96inch LCD
#define LCDSIZE 1

#include <Adafruit_GFX.h>
#if LCDSIZE == 1
  #include <Adafruit_SSD1306.h>
  #define SCREEN_WIDTH 128 // OLED display width, in pixels
  #define SCREEN_HEIGHT 64 // OLED display height, in pixels
  Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);
#else
  // for 1.3inch OLED
  #include <Adafruit_SH1106.h>
  #define OLED_RESET -1
  Adafruit_SH1106 display(OLED_RESET);
#endif

#include "LowPower.h"
#include <Adafruit_MLX90614.h>
Adafruit_MLX90614 mlx = Adafruit_MLX90614();

#include <EEPROM.h>
int addr = 42;
int UNIT = 1; //1==F, 0==C
int value;

const int buttonPin = 2;     // the number of the pushbutton pin
int buttonState = 0;

void wakeUp()
{
    // EMPTY HANDLER for WAKEUP INTERRUPT
}
void setup(void) {
  pinMode(buttonPin, INPUT_PULLUP);
  value = EEPROM.read(addr);
  if(value==1){
    UNIT=1;
    value=0;
  }
  else{
    UNIT=0;
    value=1;
  }
  if(digitalRead(buttonPin)==LOW){
    EEPROM.write(addr, value);
  }
  
#if LCDSIZE == 1
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
#else
  display.begin(SH1106_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
#endif
  display.setRotation(2);
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setTextSize(3);
  display.setCursor(0,0);
  display.println(" PAC-V");
  display.setTextSize(1);
  display.setCursor(20,24);
  display.println("NC-THERMOMETER");
  display.setCursor(14,35);
  if(UNIT==1)
    display.println("Unit: Fahrenheit");
  else
    display.println("Unit: Centigrade");
  display.setCursor(4,46);
  display.println("To change Temp. Unit");
  display.setCursor(4,55);
  display.print("Press Check at Start");
  display.display();
  display.setTextSize(4);
  mlx.begin();  
  attachInterrupt(digitalPinToInterrupt(buttonPin), wakeUp, LOW);
  LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF); 
}

float z;
char u;
void loop(void) {
  attachInterrupt(digitalPinToInterrupt(buttonPin), wakeUp, LOW);
  LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF); 
  detachInterrupt(digitalPinToInterrupt(buttonPin));
  buttonState = digitalRead(buttonPin);
  if (buttonState == LOW) {
    display.clearDisplay();
    if(UNIT==1){
      z = mlx.readObjectTempF();
      u='F';
    }
    else{
      z = mlx.readObjectTempC();
      u='C';
    }
    display.setCursor(0,0);
    display.println(z,1);
    display.setCursor(100,32);
    display.print(u);
    display.drawCircle(90, 36, 4, WHITE);
    display.display();
  }
  delay(200);
}
