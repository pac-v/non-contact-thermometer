# Non-Contact Thermometer

DIY Non Contact Thermometer for the COVID-19 Pandemic, for use by front-line healthcare workers and other medical staff as well as people at the gates of apartments, office buildings and other places where people need to move to safely and immediately check for fever symptom.

## Required Software
compiled and tested on Arduino IDE v 1.8.12

## Required Libraries
1. Adafruit MLX90614 - https://github.com/adafruit/Adafruit-MLX90614-Library
2. Adafruit SSD1306 and other dependant Adafruit GFX libraries - https://github.com/adafruit/Adafruit_SSD1306
3. Low-Power Library by Rocket Stream - https://github.com/rocketscream/Low-Power/

Libraries were installed via LIBRARY MANAGER in Arduino, which installed dependancy libraries as well.

