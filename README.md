# Non-Contact Thermometer

DIY Non Contact Thermometer for the COVID-19 Pandemic, for use by front-line healthcare workers and other medical staff as well as people at the gates of apartments, office buildings and other places where people need to move to safely and immediately check for fever symptom.

## Problem
Temperature is one of the initial and vital signs used by clinicians, and other front-line health workers to assess health status and care for patients during the Corona pandemic.  Corona virus can survive for hours in air, and for days on different surfaces. Therefore, checking temperatures via traditional body-surface contact thermometers for screening potential patients or for treating infected patients becomes problematic. Any physical contact can lead to cross-infection, and healthcare professionals are most at risk.

## Solution
The "PAC-V Non-Contact Thermometer Technical Team" has successfully created an "Open Source No-contact Infrared Thermometer (NCIT)", easily assembled by hand. NCIT a are comfortable, reliable, accurate, and easy to use with minimal training.  They are safe for use by health professionals; as they measure temperature while between 2-5cm away from the body surface, preferably the forehead.  NCITs cause less distress to patients as they measure temperature non-invasively, including while the patients are sleeping or moving.

## Target Beneficiaries
Non-contact thermometers are needed for all locations visited by a large number of people, including Clinics, Hospitals, Markets, Super Stores, Mosques and other Religious Places, and Office Buildings. Government estimates say up to 100,000 temperature checking devices are required.

## Funding Needed:
1.	Budget Required: US$ 50,000 for 1,000 Devices.  *
2.	The main component in the thermometer is a Contactless Temperature Sensor. The specific model of sensor is MLX90614ESF-DCC though its generic name is MLX90614, and a modular option is also available called GY-906.  
Please contact us if,  
a) 	you have this sensor (MLX90614 or the newer SMD variant MLX90632) in your inventory; or  
b) 	if you can have it shipped to Pakistan on an urgent basis.  
3. 	If you would like to donate the thermometers by providing financial support; then please let us know the number of thermometers and the location where they are needed. 
4. 	If you would like to build the thermometers yourself, and deliver them to hospitals in your area; then the complete designs drawings and manufacturing processes are available on GitLab Projects https://gitlab.com/pac-v. 

\* As the sensor price has increased during this pandemic due to high demand, if sensor in low price is obtained, more thermometers will be made



## Pakistan Against COVID19 Volunteers PAC-Volunteers (PAC-V)
PAC-V is a group of volunteers from across Pakistan, which has come together to develop affordable solutions to combat COVID19. The volunteers numbering over 150 are doctors, biomedical professionals, engineers, academics, diaspora, resource mobilizes, and other smaller groups. 

WEBSITE: https://pacvnow.org/


## Contacts:

 1. Dr. Bilal Siddiqui (PAC-V Initiative Lead)
+92 320 346 1598
[airbilal@gmail.com](mailto:airbilal@gmail.com)
 2. M. Zaid Pirwani (Thermometer Tech Team Lead)
+92 322 233 9026
[zaidpirwani786@gmail.com](mailto:zaidpirwani786@gmail.com)
 3. Umair Farooq, Instant3D, (Thermometer Team CAD / Mechanical Design / Enclosure )
+92 336 259 9094 / https://www.facebook.com/3dinstant/
[zaidpirwani786@gmail.com](mailto:zaidpirwani786@gmail.com)
 4. Fouad Bajwa (Media Team)
+92 333 466 1290
[fouadbajwa@gmail.com](mailto:fouadbajwa@gmail.com)
 5. Salman Khan (Media Team)
+92 321 240 0355
[salmanuiux@gmail.com](mailto:salmanuiux@gmail.com)

